package integration;

import controller.GameController;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class GameControllerTest {

    private String inputFile;
    private GameController gameController;

    @Before
    public void init() {
        inputFile = "data.txt";
        gameController = new GameController();
    }

    @Test
    public void gameExecution() throws IOException {
        gameController.excuteGame(inputFile);
    }
}
