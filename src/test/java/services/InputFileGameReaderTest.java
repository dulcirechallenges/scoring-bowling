package services;

import entities.Player;
import entities.Shot;
import exceptions.ReaderException;
import org.junit.Before;
import org.junit.Test;
import services.reader.inputfile.InputFileGameReaderJava8;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class InputFileGameReaderTest {

    private InputFileGameReaderJava8 inputFileGameReaderJava8;
    private String inputFile;

    @Before
    public void init(){
        inputFile="data.txt";
        inputFileGameReaderJava8 = new InputFileGameReaderJava8(inputFile);

    }

    @Test(expected = ReaderException.class)
    public void readFileExceptionTest() throws IOException{
        final List<Shot> expected = shotsGenerator();
        final String inputFile="dato.txt";
        final InputFileGameReaderJava8 inputFileGameReaderJava8 = new InputFileGameReaderJava8(inputFile);
        inputFileGameReaderJava8.readData();
    }

    @Test
    public void generatePlayerTest() throws IOException {
        final List<Player> expected = gameDataGenerator();
        final List<Player> actual = inputFileGameReaderJava8.playersProcesor(inputFileGameReaderJava8.readData());
        IntStream.range(0,2).forEach(index->{
            assertEquals(expected.get(index).printScores(),actual.get(index).printScores());
        });
    }

    public List<Player> gameDataGenerator(){
        final Player player1 = new Player("John",Arrays.asList("3","7","6","3","10","8","1","10","10","9","0","7","3","4","4","10","9","0"));
        final Player player2 = new Player("Jeff",Arrays.asList("10","7","3","9","0","10","0","8","8","2","F","6","10","10","10","8","1"));
        return Arrays.asList(player1,player2);
    }

    public List<Shot> shotsGenerator(){
        return Arrays.asList(new Shot("Jeff", "10"),
                new Shot("John", "3"),
                new Shot("John", "7"),
                new Shot("Jeff", "7"),
                new Shot("Jeff", "3"),
                new Shot("John", "6"),
                new Shot("John", "3"),
                new Shot("Jeff", "9"),
                new Shot("Jeff", "0"),
                new Shot("John", "10"),
                new Shot("Jeff", "10"),
                new Shot("John", "8"),
                new Shot("John", "1"),
                new Shot("Jeff", "0"),
                new Shot("Jeff", "8"),
                new Shot("John", "10"),
                new Shot("Jeff", "8"),
                new Shot("Jeff", "2"),
                new Shot("John", "10"),
                new Shot("Jeff", "F"),
                new Shot("Jeff", "6"),
                new Shot("John", "9"),
                new Shot("John", "0"),
                new Shot("Jeff", "10"),
                new Shot("John", "7"),
                new Shot("John", "3"),
                new Shot("Jeff", "10"),
                new Shot("John", "4"),
                new Shot("John", "4"),
                new Shot("Jeff", "10"),
                new Shot("Jeff", "8"),
                new Shot("Jeff", "1"),
                new Shot("John", "10"),
                new Shot("John", "9"),
                new Shot("John", "0"));
    }

}
