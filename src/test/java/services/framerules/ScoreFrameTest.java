package services.framerules;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ScoreFrameTest {

    private FrameRules frame;

    @Test
    public void rulesProcessorNumberNumber() {
        final List<Integer> pinfalls = Arrays.asList(3, 7, 5, 5, 0, 8, 8, 2, 2, 8, 10, 10, 10, 10, 3, 2);
        frame = new ScoreFrame(pinfalls);
        final List<Integer> expected = Arrays.asList(15, 25, 33, 45, 65, 95, 125, 148, 163, 168);
        assertEquals(expected, frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorStrikeSpare() {
        final List<Integer> pinfalls = Arrays.asList(3, 7, 5, 5, 0, 8, 8, 2, 2, 8, 10, 10, 10, 10, 10, 5, 5);
        frame = new ScoreFrame(pinfalls);
        final List<Integer> expected = Arrays.asList(15, 25, 33, 45, 65, 95, 125, 155, 180, 200);
        assertEquals(expected, frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorStrikeStrikeStrike() {
        final List<Integer> pinfalls = Arrays.asList(3, 7, 5, 5, 0, 8, 8, 2, 2, 8, 10, 10, 10, 10, 10, 10, 10);
        frame = new ScoreFrame(pinfalls);
        final List<Integer> expected = Arrays.asList(15, 25, 33, 45, 65, 95, 125, 155, 185, 215);
        assertEquals(expected, frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorStrikeSpareNumber() {
        final List<Integer> pinfalls = Arrays.asList(3, 7, 5, 5, 0, 8, 8, 2, 2, 8, 10, 10, 10, 10, 3, 7, 2);
        frame = new ScoreFrame(pinfalls);
        final List<Integer> expected = Arrays.asList(15, 25, 33, 45, 65, 95, 125, 148, 168, 180);
        assertEquals(expected, frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorSpareStrike() {
        final List<Integer> pinfalls = Arrays.asList(3, 7, 5, 5, 0, 8, 8, 2, 2, 8, 10, 10, 10, 10, 3, 7, 10);
        frame = new ScoreFrame(pinfalls);
        final List<Integer> expected = Arrays.asList(15, 25, 33, 45, 65, 95, 125, 148, 168, 188);
        assertEquals(expected, frame.rulesProcessor(0));
    }


}