package services.framerules;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PrintLastFrameTest {

    private FrameRules frame;

    @Test
    public void rulesProcessorNumberNumber(){
        final List<Integer> pinfalls = Arrays.asList(3,7,5,5,0,8,8,2,2,8,10,10,10,10,3,2);
        frame=new PrintLastFrame(pinfalls);
        final String expected ="\t3\t2";
        frame.rulesProcessor(14);
        assertEquals(expected,frame.rulesProcessor(14));
    }

    @Test
    public void rulesProcessorStrikeSpare(){
        final List<Integer> pinfalls = Arrays.asList(3,7,5,5,0,8,8,2,2,8,10,10,10,10,10,5,5);
        frame=new PrintLastFrame(pinfalls);
        final String expected ="\tX\t5\t/";
        frame.rulesProcessor(14);
        assertEquals(expected,frame.rulesProcessor(14));
    }

    @Test
    public void rulesProcessorStrikeStrikeStrike(){
        final List<Integer> pinfalls = Arrays.asList(3,7,5,5,0,8,8,2,2,8,10,10,10,10,10,10,10);
        frame=new PrintLastFrame(pinfalls);
        final String expected ="\tX\tX\tX";
        frame.rulesProcessor(14);
        assertEquals(expected,frame.rulesProcessor(14));
    }

    @Test
    public void rulesProcessorStrikeSpareNumber(){
        final List<Integer> pinfalls = Arrays.asList(3,7,5,5,0,8,8,2,2,8,10,10,10,10,3,7,2);
        frame=new PrintLastFrame(pinfalls);
        final String expected ="\t3\t/\t2";
        frame.rulesProcessor(14);
        assertEquals(expected,frame.rulesProcessor(14));
    }

    @Test
    public void rulesProcessorSpareStrike(){
        final List<Integer> pinfalls = Arrays.asList(3,7,5,5,0,8,8,2,2,8,10,10,10,10,3,7,10);
        frame=new PrintLastFrame(pinfalls);
        final String expected ="\t3\t/\tX";
        frame.rulesProcessor(14);
        assertEquals(expected,frame.rulesProcessor(14));
    }

}