package services.framerules;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NormalFrameTest {

    private FrameRules frame;

    @Test
    public void rulesProcessorNumberNumber(){
        final List<String> pinfalls = Arrays.asList("3","7","5","5","0","8","8","2","2","8","10","10","10","10","3","2");
        frame=new PrintFrame(pinfalls);
        final String expected ="Pinfalls\t\t3\t/\t5\t/\t0\t8\t8\t/\t2\t/\t\tX\t\tX\t\tX\t\tX\t3\t2";
        assertEquals(expected,frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorStrikeSpare(){
        final List<String> pinfalls = Arrays.asList("3","7","5","5","0","8","8","2","2","8","10","10","10","10","10","5","5");
        frame=new PrintFrame(pinfalls);
        final String expected ="Pinfalls\t\t3\t/\t5\t/\t0\t8\t8\t/\t2\t/\t\tX\t\tX\t\tX\t\tX\tX\t5\t/";
        assertEquals(expected,frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorStrikeStrikeStrike(){
        final List<String> pinfalls = Arrays.asList("3","7","5","5","0","8","8","2","2","8","10","10","10","10","10","10","10");
        frame=new PrintFrame(pinfalls);
        final String expected ="Pinfalls\t\t3\t/\t5\t/\t0\t8\t8\t/\t2\t/\t\tX\t\tX\t\tX\t\tX\tX\tX\tX";
        assertEquals(expected,frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorStrikeSpareNumber(){
        final List<String> pinfalls = Arrays.asList("3","7","5","5","0","8","8","2","2","8","10","10","10","10","3","7","2");
        frame=new PrintFrame(pinfalls);
        final String expected ="Pinfalls\t\t3\t/\t5\t/\t0\t8\t8\t/\t2\t/\t\tX\t\tX\t\tX\t\tX\t3\t/\t2";
        assertEquals(expected,frame.rulesProcessor(0));
    }

    @Test
    public void rulesProcessorSpareStrike(){
        final List<String> pinfalls = Arrays.asList("3","7","5","5","0","8","8","2","2","8","10","10","10","10","3","7","10");
        frame=new PrintFrame(pinfalls);
        final String expected ="Pinfalls\t\t3\t/\t5\t/\t0\t8\t8\t/\t2\t/\t\tX\t\tX\t\tX\t\tX\t3\t/\tX";
        assertEquals(expected,frame.rulesProcessor(0));
    }

}