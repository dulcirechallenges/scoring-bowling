package controller;

import entities.Player;
import services.GameService;
import services.OutputGamePrinter;
import services.reader.inputfile.InputFileGameReader;
import services.reader.inputfile.InputFileGameReaderJava8;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GameController {

    private static final Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void excuteGame(final String input) throws IOException {
        LOGGER.log(Level.INFO,String.format("Processing data from %s",input));
        final InputFileGameReader inputFileGameReader = new InputFileGameReaderJava8(input);
        final List<Player> players = inputFileGameReader.execute();
        LOGGER.log(Level.INFO,String.format("Processed data from %s",input));
        LOGGER.log(Level.INFO,String.format("Scoring Game"));
        final GameService gameService = new GameService(players);
        final List<String> gameResults = gameService.gameResults();
        LOGGER.log(Level.INFO,String.format("Game Scored"));
        LOGGER.log(Level.INFO,String.format("Saving Result"));
        final OutputGamePrinter gamePrinter = new OutputGamePrinter(gameResults);
        gamePrinter.printFile();
        LOGGER.log(Level.INFO,String.format("Result Saved"));
    }
}
