package exceptions;

import java.io.IOException;

public class ReaderException extends IOException {
    public ReaderException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ReaderException(final String message) {
        super(message);
    }
}
