import controller.GameController;

import java.io.IOException;
import java.util.Arrays;

public class ScoringBowling {

    public static void main(final String []args) throws IOException {
        final GameController game = new GameController();
        game.excuteGame(Arrays.stream(args).findFirst().get());
    }
}
