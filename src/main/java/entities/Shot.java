package entities;

import exceptions.ReaderException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Shot {

    private String name;
    private String pinFall;


    @SneakyThrows
    public static Shot generateShot(final String[] data) throws ReaderException {
        try {
            return new Shot(data[0], data[1]);
        } catch (RuntimeException e) {
            throw new ReaderException(String.format("Incorrect Format. Each play must be in a diferent line and use only tab to separate player from its score"));
        }
    }

    @Override
    public String toString() {
        return String.format("Shot{name=%s , pinfall=%s}",name,pinFall);
    }

    public boolean verifyPinFall() throws ReaderException {
        if (!pinFall.equals("F")) {
            try {
                final Integer shot = Integer.parseInt(pinFall);
                return shot.compareTo(10) <= 0 && shot.compareTo(0) >= 0;
            } catch (Exception e) {
                throw new ReaderException(String.format("There are incorrect puntuation in your input %s", this));
            }
        }
        return true;
    }

    public void validationScore() throws ReaderException {
        if (!verifyPinFall()) {
            throw new ReaderException(String.format("There are incorrect puntuation in your input %s", this));
        }
    }
}
