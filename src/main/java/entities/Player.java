package entities;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class Player {

    private String name;
    private List<String> pinFalls;
    private List<Integer> scores;

    public Player(final String name, final List<String> pinFalls) {
        this.name = name;
        this.pinFalls = pinFalls;
        scores = new ArrayList<Integer>();
    }

    public String printNamePlayer() {
        return name;
    }

    public String printScores() {
        final StringBuilder result = new StringBuilder();
        scores.stream().forEach(score -> result.append("\t\t").append(score));
        return result.toString();
    }

    public List<String> getPinFalls() {
        return pinFalls;
    }

    public List<Integer> pinFallsValues() {
        final List<String> copy = new ArrayList<>(pinFalls);
        Collections.replaceAll(copy, "F", "0");
        return copy.stream().map(Integer::parseInt).collect(Collectors.toList());
    }

    public void insertScores(final List<Integer> scores) {
        this.scores = scores;
    }

}
