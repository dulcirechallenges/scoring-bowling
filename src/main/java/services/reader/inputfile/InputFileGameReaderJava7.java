package services.reader.inputfile;

import entities.Shot;
import exceptions.ReaderException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InputFileGameReaderJava7 extends InputFileGameReader<Shot, String> {

    private static final Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public InputFileGameReaderJava7(final String input) {
        super(input);
    }

    public List<Shot> readDataFile() throws ReaderException {
        FileReader fr = null;
        final List<Shot> gameData = new ArrayList<>();
        try {
            fr = new FileReader(new File(super.input));
            final BufferedReader br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                gameData.add(inputLecture(linea, "\t"));
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, String.format("input file %s not processed. Exception: %s", input, e));
            throw new ReaderException("input file not processed", e.getCause());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return gameData;
    }

    public Shot inputLecture(final String linea, final String regex) {
        try {
            return Shot.generateShot(linea.split(regex));
        } catch (ReaderException e) {
            LOGGER.log(Level.WARNING, String.format("input file %s not processed. Exception: %s", super.input, e));
        }
        return new Shot();

    }


}
