package services.reader.inputfile;

import entities.Shot;
import exceptions.ReaderException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class InputFileGameReaderJava8 extends InputFileGameReader<List<?>, Stream<String>> {
    private static final Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public InputFileGameReaderJava8(final String input) {
        super(input);
    }

    public List<Shot> readDataFile() throws ReaderException {
        List<Shot> gameData = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(inputFileName(input)))) {
            LOGGER.log(Level.INFO, String.format("Processing input file %s", input));
            gameData = (List<Shot>) inputLecture(stream, "\t");
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, String.format("input file %s not processed. Exception: %s", input, e));
            throw new ReaderException("input file not processed", e.getCause());
        }
        return gameData;
    }


    public List<?> inputLecture(final Stream<String> data, final String regex) {
        return data.map(linea -> linea.split(regex))
                .map((String[] play) -> {
                    try {
                        return Shot.generateShot(play);
                    } catch (ReaderException e) {
                        LOGGER.log(Level.WARNING, String.format("input file %s not processed. Exception: %s", input, e));
                    }
                    return new Shot();
                })
                .collect(Collectors.toList());
    }

    private String inputFileName(final String data) {
        return String.format("C:\\gameinput\\%s", data);
    }

}
