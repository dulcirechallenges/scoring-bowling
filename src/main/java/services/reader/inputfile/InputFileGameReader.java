package services.reader.inputfile;

import entities.Player;
import entities.Shot;
import exceptions.ReaderException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import services.reader.InputGameReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
public abstract class InputFileGameReader<T, K> implements InputGameReader {

    protected String input;


    public abstract List<Shot> readDataFile() throws ReaderException;

    public abstract T inputLecture(final K data, final String regex);

    public List<Player> execute() throws IOException {
        final List<Shot> gameData = readData();
        validationPinFall(gameData);
        return playersProcesor(gameData);
    }

    public List<Shot> readData() throws IOException {
        final List<Shot> gameData = readDataFile();
        return gameData;
    }

    public List<Player> playersProcesor(final List<Shot> shots) {
        final List<Player> players = new ArrayList<>();
        shots.stream()
                .collect(Collectors.groupingBy(Shot::getName, Collectors.mapping(Shot::getPinFall, Collectors.toList())))
                .entrySet().forEach(game -> {
                    players.add(new Player(game.getKey(), game.getValue()));
                });
        return players;
    }

    public void validationPinFall(final List<Shot> gameData) throws ReaderException {
        for (Shot shot : gameData) {
            shot.validationScore();
        }
    }

}
