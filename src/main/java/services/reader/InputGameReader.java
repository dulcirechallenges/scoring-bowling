package services.reader;

import entities.Player;
import entities.Shot;

import java.util.List;

public interface InputGameReader {

    List<Player> execute() throws Exception;

    List<Shot> readData() throws Exception;

    List<Player> playersProcesor(final List<Shot> shots);
}
