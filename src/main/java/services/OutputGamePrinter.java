package services;

import exceptions.ReaderException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@AllArgsConstructor
@NoArgsConstructor
public class OutputGamePrinter {

    private List<String> results;

    private static final Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void printFile() throws IOException {
        final String pathName = outputFileName();
        final Path path = Paths.get(pathName);
        try (BufferedWriter br = Files.newBufferedWriter(path,
                Charset.defaultCharset(), StandardOpenOption.CREATE)) {
            results.forEach((s) -> {
                try {
                    br.write(s);
                    br.newLine();
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });
            LOGGER.log(Level.INFO, String.format("Game results printed in file %s", pathName));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, String.format("output data not processed. Exception: %s", e));
            throw new ReaderException("output data not processed", e.getCause());
        }
    }

    private String outputFileName() {
        return String.format("C:\\gameresults\\results-%s.txt", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
    }


}
