package services;

import entities.Player;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@AllArgsConstructor
public class GameService {

    private PlayerService playerService;
    private List<Player> players;

    public GameService(final List<Player> players) {
        this.players = players;
        this.playerService = new PlayerService();
    }

    public List<String> gameResults() {
        final List<String> gameResults = new ArrayList<>();
        gameResults.add(printHeader());
        players.stream().forEach(player -> {
            gameResults.addAll(playerService.playerResults(player));
        });
        return gameResults;
    }

    public String printHeader() {
        final StringBuilder result = new StringBuilder();
        result.append("Frame\t\t");
        IntStream.range(1, 11).forEach(i -> {
            result.append("\t\t").append(i);
        });
        return result.toString();
    }
}
