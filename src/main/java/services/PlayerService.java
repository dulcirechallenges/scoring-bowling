package services;

import entities.Player;
import services.framerules.FrameRules;
import services.framerules.PrintFrame;
import services.framerules.ScoreFrame;

import java.util.Arrays;
import java.util.List;


public class PlayerService {

    private FrameRules frameRules;

    public void scoringPlayer(final Player player) {
        frameRules = new ScoreFrame(player.pinFallsValues());
        final List<Integer> scores = (List<Integer>)frameRules.rulesProcessor(0);
        player.insertScores(scores);
    }

    public String printPinfallsPlayer(final Player player) {
        frameRules = new PrintFrame(player.getPinFalls());
        return (String) frameRules.rulesProcessor(0);
    }

    public String printPlayerName(final Player player) {
        return player.printNamePlayer();
    }

    public String printPlayerScores(final Player player) {
        final StringBuilder result = new StringBuilder();
        return result.append("Score\t\t").append(player.printScores()).toString();
    }

    public List<String> playerResults(final Player player) {
        scoringPlayer(player);
        return Arrays.asList(printPlayerName(player), printPinfallsPlayer(player), printPlayerScores(player));

    }


}
