package services.framerules;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class PrintLastFrame implements FrameRules {

    private List<Integer> pinFalls;

    @Override
    public String returnStrike(final Integer shot) {
        final StringBuilder result = new StringBuilder();
        return result.append("\t").append("X").toString();
    }

    @Override
    public String returnSpare(final Integer shot) {
        final StringBuilder result = new StringBuilder();
        return result.append("\t").append(pinFalls.get(shot))
                .append("\t/")
                .toString();
    }

    @Override
    public String returnFrame(final Integer shot) {
        final StringBuilder result = new StringBuilder();
        result.append("\t")
                .append(pinFalls.get(shot));
        return result.toString();
    }

    public boolean isStrike(final Integer shot) {
        final List<Integer> hits = pinFalls;
        return hits.get(shot).equals(10);
    }

    public boolean isSpare(final Integer shot) {
        final List<Integer> pinFalls = this.pinFalls;
        return (shot + 1) != this.pinFalls.size() && (pinFalls.get(shot) + pinFalls.get(shot + 1)) == 10;
    }

    public String rulesProcessor(final Integer shotGame) {
        final StringBuilder result = new StringBuilder();
        Integer shot = shotGame;
        if (shot.compareTo(pinFalls.size()) < 0) {
            if (isStrike(shot)) {
                result.append(returnStrike(shot));
                shot++;
            } else if (isSpare(shot)) {
                result.append(returnSpare(shot));
                shot += 2;
            } else {
                result.append(returnFrame(shot));
                shot++;
            }
            result.append(rulesProcessor(shot));
        }

        return result.toString();
    }

}
