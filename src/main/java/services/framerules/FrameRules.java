package services.framerules;

public interface FrameRules<T> {

    T returnStrike(final Integer shot);

    T returnSpare(final Integer shot);

    T returnFrame(final Integer shot);

    boolean isStrike(final Integer shot);

    boolean isSpare(final Integer shot);

    T rulesProcessor(final Integer shot);
}
