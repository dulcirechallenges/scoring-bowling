package services.framerules;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class PrintFrame implements FrameRules {

    private List<String> pinFalls;


    @Override
    public String returnStrike(final Integer shot) {
        final StringBuilder result = new StringBuilder();
        return result.append("\t\t").append("X").toString();
    }

    @Override
    public String returnSpare(final Integer shot) {
        final StringBuilder result = new StringBuilder();
        return result.append("\t").append(pinFalls.get(shot))
                .append("\t/")
                .toString();
    }

    @Override
    public String returnFrame(final Integer shot) {
        final StringBuilder result = new StringBuilder();
        result.append("\t")
                .append(pinFalls.get(shot))
                .append("\t").append(pinFalls.get(shot + 1));
        return result.toString();
    }

    @Override
    public boolean isStrike(final Integer shot) {
        final List<Integer> hits = pinFallsValues();
        return hits.get(shot).equals(10);
    }

    @Override
    public boolean isSpare(final Integer shot) {
        final List<Integer> pinFalls = pinFallsValues();
        return pinFalls.get(shot) + pinFalls.get(shot + 1) == 10;
    }

    @Override
    public String rulesProcessor(final Integer shotGame) {
        final FrameRules lastFrame = new PrintLastFrame(pinFallsValues());
        final StringBuilder result = new StringBuilder();
        int lastShot = 0;
        int shot = shotGame;
        result.append("Pinfalls\t");
        for (int frame = 0; frame < 8; frame++) {

            if (isStrike(shot)) {
                result.append(returnStrike(shot));
                shot++;
            } else if (isSpare(shot)) {
                result.append(returnSpare(shot));
                shot += 2;
            } else {
                result.append(returnFrame(shot));
                shot += 2;
            }

            if (frame == 7) {
                lastShot = shot;
            }
        }
        result.append("\t");
        result.append(lastFrame.rulesProcessor(lastShot));
        return result.toString();
    }

    public List<Integer> pinFallsValues() {
        final List<String> copy = new ArrayList<>(pinFalls);
        Collections.replaceAll(copy, "F", "0");
        return copy.stream().map(Integer::parseInt).collect(Collectors.toList());
    }
}
