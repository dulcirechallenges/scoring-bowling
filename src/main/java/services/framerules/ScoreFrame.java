package services.framerules;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class ScoreFrame implements FrameRules {

    private List<Integer> pinFalls;

    @Override
    public Integer returnStrike(final Integer shot) {
        return 10 + pinFalls.get(shot + 1) + pinFalls.get(shot + 2);
    }

    @Override
    public Integer returnSpare(final Integer shot) {
        return 10 + pinFalls.get(shot + 2);
    }

    @Override
    public Integer returnFrame(final Integer shot) {
        return pinFalls.get(shot) + pinFalls.get(shot + 1);
    }

    @Override
    public boolean isStrike(final Integer shot) {
        final List<Integer> hits = pinFalls;
        return hits.get(shot).equals(10);
    }

    @Override
    public boolean isSpare(final Integer shot) {
        final List<Integer> pinFalls = this.pinFalls;
        return pinFalls.get(shot) + pinFalls.get(shot + 1) == 10;
    }

    @Override
    public List<Integer> rulesProcessor(final Integer shotGame) {
        final List<Integer> score = new ArrayList<>();
        final StringBuilder result = new StringBuilder();
        int shot = shotGame;
        int acumScore = 0;
        result.append("Pinfalls\t");
        for (int frame = 0; frame < 10; frame++) {
            if (isStrike(shot)) {
                acumScore += returnStrike(shot);
                score.add(acumScore);
                shot++;
            } else if (isSpare(shot)) {
                acumScore += returnSpare(shot);
                score.add(acumScore);
                shot += 2;
            } else {
                acumScore += returnFrame(shot);
                score.add(acumScore);
                shot += 2;
            }

        }
        return score;
    }
}
