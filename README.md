# README #

### Repository Information ###

* Java Challenge Jobsity
* Version 1.0.0
* https://Dulcire@bitbucket.org/dulcirechallenges/scoring-bowling.git

### How do I get set up? ###


* Summary of set up
* Configuration
  N/A
* Dependencies
    projectlombok
* Database configuration:
    For this challegen the data to process should be readed and printed in file texts 
    You must create a folder in your path C: named gameresults
    Should look like "C:\gameresults"
    You must create a folder in your path C: named gameresults
    Should look like "C:\gameinput"
    In this folder you must crate your data input text file. 
    This data should follow the requirements which were stablished into the java challege sent.

* Deployment instructions
    1)Unzip the file scoring-bowling.zip
    2)Open the bash in your folder unziped
    3)Run the followin command : 
        java -cp scoring-bowling-1.0-SNAPSHOT.jar ScoringBowling data.txt
        Note: Your data.txt should be in "C:\gameinput" (Database configuration)
    4)Check the Game results in "C:\gameresults"
    